# Bookshop README#
This bookshop application uses XML files to load in a selection of books and discounts. A bookshop scenario is simulated where specified discounts are applied to individual items as well as the basket of items. Below, the application guide and XML format is documented.
## How to use the application? ##
* Build Release version of project
* Run Executable from release folder
* Any incorrect values entered into the application will be caught and the user notified of incorrect value entry (eg negative numbers, numbers outside of ID range listed, string values)
* Any items listed in store and basket with a discount already have that percentage removed from the listed price in the store and basket
### Store ###
* Enter one of the listed IDs for books to add to the basket
* Enter 0 to view the basket
### Basket ###
* Enter one of the listed IDs for books to remove the item from the basket
* Enter 0 to return to the store page
* Any basket discounts listed are already applied to the final total
* Only one basket discount is allowed at a time
* If two basket discounts are met, the first basket discount in the XML file for discounts will be applied (see below for XML format)
## XML Format ##
### Books ###
* Must contain a title, year and price in the format: <Book Title="Moby Dick" Year="1851" Price="15.20"/>
* Year must be prior to current year to avoid books from the future
* Prices are to 2 decimal places
### Discounts ###
* Two types of discount exist: Item Discount; Basket Discount
#### Item Discount ####
* Item discounts depend on either the year of the item or the title of the item
* For the year of the item, the condition must be a year. For the discount to apply, the book must be =, <, >, <= or >= to the specified condition
* For the title of the item, the condition must be a title in a string format. For the discount to apply, the book title must exactly match the condition
* The percent discount must be specified between and including 1-100
Year condition example: 	<Discount Application="Item"   Type="Year"   Condition1="2000"   Condition2="<"   Percent="10"/>
Title condition example: 	<Discount Application="Item"   Type="Title"   Condition1="A Christmas Carol"   Condition2="="   Percent="25"/>
#### Basket Discount ####
* Basket Discounts depend on the total cost of the basket or the number of items in the basket
* For the cost (amount) of the basket, the condition must be a 2 decimal floating point number specifying an amount. For the discount to apply, the amount in the basket must be =, <, >, <= or >= to the specified condition
* For the number of items (count) in the basket, the condition must be an integer. For the discount to apply, the number of items in the basket must be =, <, >, <= or >= to the specified condition
* The percent discount must be specified between and including 1-100
Cost (amount) condition example: 	<Discount Application="Basket"   Type="Amount"   Condition1="30.00"   Condition2="<"   Percent="5"/>
Number of items (count) condition example: 	<Discount Application="Basket"   Type="Count"   Condition1="5"   Condition2="<="   Percent="50"/>