#include <Basket.h>
#include <iostream>
#include <Windows.h>

Basket::Basket(){ total = 0.0f; }

void Basket::AddItem(Book* book)
{
	books.push_back(book);
}

void Basket::RemoveItem(int index)
{
	try
	{
		std::string name = books.at(index)->GetTitle();
		books.erase(books.begin() + index);
		std::cout << name << " was removed from the basket!";
		Sleep(2000);
	}
	catch (std::exception e)
	{
		std::cout << "Invalid ID entered, no action taken!";
		Sleep(2000);
	}
}

void Basket::CalculateTotal()
{
	total = 0.0f;
	for each (Book* book in books)
	{
		total += book->GetPrice();
	}
	count = books.size();

	for each (BasketDiscount* discount in discounts)
	{
		if (discount->ApplyDiscount(total, count))
			break;
	}

	std::cout << "\n\nTotal amount due: \x9C" << total;
}

void Basket::AddDiscount(BasketDiscount* discount)
{
	discounts.push_back(discount);
}

std::vector<Book*> Basket::GetBooksInBasket()
{
	return books;
}