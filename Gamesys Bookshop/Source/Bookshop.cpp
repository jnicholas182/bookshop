#include <Bookshop.h>
#include <BasketDiscount.h>
#include <rapidxml.hpp>
#include <rapidxml_utils.hpp>
#include <iostream>
#include <Windows.h>

using namespace rapidxml;
using namespace std;

Bookshop::Bookshop()
{
	basket = new Basket();
	//Load book data from XML file
	file<> booksXMLFile("Books.xml");
	xml_document<> booksXML;
	booksXML.parse<0>(booksXMLFile.data());

	for (xml_node<> *bookNode = booksXML.first_node()->first_node("Book"); bookNode; bookNode = bookNode->next_sibling())
	{
		Book* book = new Book;
		if (book->Init(bookNode->first_attribute()->value(),
					   bookNode->first_attribute()->next_attribute()->value(),
					   bookNode->first_attribute()->next_attribute()->next_attribute()->value()))
			books.push_back(book);
	}
	//Find longest title for use in formatting of strings in store
	longestStringLength = 0;
	for each(Book* book in books)
	{
		if (book->GetTitle().length() > longestStringLength)
			longestStringLength = book->GetTitle().length();
	}

	//Load book data from XML file
	file<> discountsXMLFile("Discounts.xml");
	xml_document<> discountsXML;
	discountsXML.parse<0>(discountsXMLFile.data());

	for (xml_node<> *discountNode = discountsXML.first_node()->first_node("Discount"); discountNode; discountNode = discountNode->next_sibling())
	{
		std::string discountApplication = discountNode->first_attribute()->value();
		if (discountApplication == "Item")
		{
			ItemDiscount* discount = new ItemDiscount();
			if(discount->Init(discountNode->first_attribute()->next_attribute()->value(),
						   discountNode->first_attribute()->next_attribute()->next_attribute()->value(),
						   discountNode->first_attribute()->next_attribute()->next_attribute()->next_attribute()->value(),
						   stoi(discountNode->first_attribute()->next_attribute()->next_attribute()->next_attribute()->next_attribute()->value())))
				itemDiscounts.push_back(discount);
		}
		else if (discountApplication == "Basket")
		{
			BasketDiscount* discount = new BasketDiscount();
			if (discount->Init(discountNode->first_attribute()->next_attribute()->value(),
				discountNode->first_attribute()->next_attribute()->next_attribute()->value(),
				discountNode->first_attribute()->next_attribute()->next_attribute()->next_attribute()->value(),
				stoi(discountNode->first_attribute()->next_attribute()->next_attribute()->next_attribute()->next_attribute()->value())))
				basket->AddDiscount(discount);
		}
	}

	for each(ItemDiscount* itemDiscount in itemDiscounts)
		itemDiscount->ApplyDiscounts(books);
}

void Bookshop::Run()
{
	bool finishedInStore = false;
	while (!finishedInStore)
	{
		system("cls");
		cout << "===============================================================================\n"
			 << "                                The Bookshop                                   \n"
			 << "===============================================================================\n"
			 << "ID\tTitle\t\t\t\t\tYear\tPrice\tDiscount\n"
			 << "-------------------------------------------------------------------------------\n";

		int id = 1;
		for each (Book* book in books)
		{
			cout << id << "\t";
			book->Print(longestStringLength);
			id++;
		}
		cout << "\n\nPlease enter a book ID to add to the cart or enter 0 to proceed to checkout: ";
		try
		{
			string input;
			getline(cin, input);
			unsigned int enteredID = stoi(input);
			if (enteredID == 0)
			{
				finishedInStore = true;
				break;
			}
			else if (enteredID > books.size())
			{
				std::cout << "Invalid ID entered, no action taken!";
				Sleep(1000);
			}
			else
			{
				enteredID--;
				basket->AddItem(books[enteredID]);
				cout << "Added " << books[enteredID]->GetTitle() << " to your basket!";
				Sleep(1000);
			}
		}
		catch (exception e)
		{
			std::cout << "Invalid ID entered, no action taken!";
			Sleep(1000);
		}
	}

	Checkout();
}

void Bookshop::Checkout()
{
	bool finishedInCheckout = false;
	while (!finishedInCheckout)
	{
		system("cls");
		cout << "===============================================================================\n"
			 << "                       Checkout - Items in your basket                         \n"
			 << "===============================================================================\n"
			 << "ID\tTitle\t\t\t\t\tYear\tPrice\tDiscount\n"
			 << "-------------------------------------------------------------------------------\n";

		unsigned int id = 1;
		for each (Book* book in basket->GetBooksInBasket())
		{
			cout << id << "\t";
			book->Print(longestStringLength);
			id++;
		}
		basket->CalculateTotal();
		cout << "\n\nTo remove an item, enter the corresponding ID or enter 0 to return to the store: ";
		try
		{
			string input;
			getline(cin, input);
			int enteredID = stoi(input);
			if (enteredID == 0)
			{
				Run();
			}
			else
			{
				enteredID--;
				basket->RemoveItem(enteredID);
			}
		}
		catch (exception e)
		{
			std::cout << "Invalid ID entered, no action taken!";
			Sleep(1000);
		}
	}
}

void Bookshop::Stop()
{

}