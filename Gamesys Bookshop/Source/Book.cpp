#include <Book.h>
#include <iostream>
#include <iomanip>

Book::Book(){}

bool Book::Init(std::string title, std::string year, std::string price)
{
	if (title == "" || year == "" || price == "")
		return false;
	this->title = title;
	try
	{
		this->year = std::stoi(year);
		
		const time_t now = time(0);
		tm * local = new tm();
		localtime_s(local, &now);
		if (local->tm_year + 1900 < this->year)
			throw std::invalid_argument("");

		this->price = std::stof(price);
	}
	catch (std::exception e)
	{
		return false;
	}
	discount = 0;
	return true;
}

void Book::ApplyDiscount(int percent)
{
	discount = percent;
	price -= price * (percent / 100.0f);
}

std::string Book::GetTitle()
{
	return title;
}

int Book::GetYear()
{
	return year;
}

float Book::GetPrice()
{
	return price;
}

void Book::Print(int longestStringLength)
{
	std::string resizedTitle = title;
	resizedTitle.resize(longestStringLength, ' ');
	std::cout << resizedTitle << "\t";
	std::cout << year << "\t";
	std::cout << "\x9C" << std::fixed << std::setprecision(2) << price;
	if (discount > 0)
		std::cout << "\t-" << discount << "%\n";
	else
		std::cout << "\n";
}