#include <BasketDiscount.h>
#include <iostream>

BasketDiscount::BasketDiscount(){}

bool BasketDiscount::Init(std::string type, std::string condition1, std::string condition2, int percent)
{
	if (type == "Amount")
		discountType = BasketDiscountType::Amount;
	else if (type == "Count")
		discountType = BasketDiscountType::Count;
	else
		return false;

	this->condition1 = condition1;
	if (percent > 0 && percent < 101)
		this->percent = percent;
	else
		return false;

	std::size_t found = condition2.find('=');
	if (found != std::string::npos)
	{
		if (condition2.size() == 1)
		{
			this->condition2 = DiscountCondition::Equal;
			return true;
		}
		found = condition2.find('<');
		if (found != std::string::npos)
		{
			this->condition2 = DiscountCondition::GreaterThanEqualTo;
			return true;
		}
		else
		{
			found = condition2.find('>');
			if (found != std::string::npos)
			{
				this->condition2 = DiscountCondition::LessThanEqualTo;
				return true;
			}
			else
				return false;
		}
	}
	else
	{
		found = condition2.find('<');
		if (found != std::string::npos)
		{
			this->condition2 = DiscountCondition::GreaterThan;
			return true;
		}
		else
		{
			found = condition2.find('>');
			if (found != std::string::npos)
			{
				this->condition2 = DiscountCondition::LessThan;
				return true;
			}
			else
				return false;
		}
	}
}

bool BasketDiscount::ApplyDiscount(float &total, int count)
{
	switch (discountType)
	{
	case Amount:
		switch (condition2)
		{
		case Equal:
			if (std::stof(condition1) == total)
			{
				std::cout << "\nDiscount: " << percent << "% \tReason: " << std::stof(condition1) << " worth of items in basket";
				total -= (total * (percent / 100.0f));
				return true;
			}
			break;
		case GreaterThan:
			if (std::stof(condition1) < total)
			{
				std::cout << "\nDiscount: " << percent << "% \tReason: More than " << std::stof(condition1) << " worth of items in basket";
				total -= (total * (percent / 100.0f));
				return true;
			}
			break;
		case LessThan:
			if (std::stof(condition1) > total)
			{
				std::cout << "\nDiscount: " << percent << "% \tReason: Less than " << std::stof(condition1) << " worth of items in basket";
				total -= (total * (percent / 100.0f));
				return true;
			}
			break;
		case GreaterThanEqualTo:
			if (std::stof(condition1) <= total)
			{
				std::cout << "\nDiscount: " << percent << "% \tReason: " << std::stof(condition1) << " or more worth of items in basket";
				total -= (total * (percent / 100.0f));
				return true;
			}
			break;
		case LessThanEqualTo:
			if (std::stof(condition1) >= total)
			{
				std::cout << "\nDiscount: " << percent << "% \tReason: " << std::stof(condition1) << " or less worth of items in basket";
				total -= (total * (percent / 100.0f));
				return true;
			}
			break;
		default:
			return false;
			break;
		}

		break;
	case Count:
		switch (condition2)
		{
		case Equal:
			if (std::stoi(condition1) == count)
			{
				std::cout << "\nDiscount: " << percent << "% \tReason: " << std::stoi(condition1) << " items in basket";
				total -= (total * (percent / 100.0f));
				return true;
			}
			break;
		case GreaterThan:
			if (std::stoi(condition1) < count)
			{
				std::cout << "\nDiscount: " << percent << "% \tReason: More than " << std::stoi(condition1) << " items in basket";
				total -= (total * (percent / 100.0f));
				return true;
			}
			break;
		case LessThan:
			if (std::stoi(condition1) > count)
			{
				std::cout << "\nDiscount: " << percent << "% \tReason: Less than " << std::stoi(condition1) << " items in basket";
				total -= (total * (percent / 100.0f));
				return true;
			}
			break;
		case GreaterThanEqualTo:
			if (std::stoi(condition1) <= count)
			{
				std::cout << "\nDiscount: " << percent << "% \tReason: " << std::stoi(condition1) << " or more items in basket";
				total -= (total * (percent / 100.0f));
				return true;
			}
			break;
		case LessThanEqualTo:
			if (std::stoi(condition1) >= count)
			{
				std::cout << "\nDiscount: " << percent << "% \tReason: " << std::stoi(condition1) << " or less items in basket";
				total -= (total * (percent / 100.0f));
				return true;
			}
			break;
		default:
			return false;
			break;
		}
		break;
	default:
		return false;
		break;
	}
	return false;
}