#include <iostream>
#include <Bookshop.h>

using namespace std;

int main()
{
	Bookshop* bookshop = new Bookshop();
	bookshop->Run();
	bookshop->Stop();
	delete bookshop;
}