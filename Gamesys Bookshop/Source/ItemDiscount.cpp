#include <ItemDiscount.h>

ItemDiscount::ItemDiscount(){}

bool ItemDiscount::Init(std::string type, std::string condition1, std::string condition2, int percent)
{
	if (percent > 0 && percent < 101)
		this->percent = percent;
	else
		return false;

	if (type == "Year")
	{
		discountType = ItemDiscountType::Year;
		this->condition1 = condition1;
		std::size_t found = condition2.find('=');
		if (found != std::string::npos)
		{
			if (condition2.size() == 1)
			{
				this->condition2 = DiscountCondition::Equal;
				return true;
			}
			found = condition2.find('<');
			if (found != std::string::npos)
			{
				this->condition2 = DiscountCondition::GreaterThanEqualTo;
				return true;
			}
			else
			{
				found = condition2.find('>');
				if (found != std::string::npos)
				{
					this->condition2 = DiscountCondition::LessThanEqualTo;
					return true;
				}
				else
					return false;
			}
		}
		else
		{
			found = condition2.find('<');
			if (found != std::string::npos)
			{
				this->condition2 = DiscountCondition::GreaterThan;
				return true;
			}
			else
			{
				found = condition2.find('>');
				if (found != std::string::npos)
				{
					this->condition2 = DiscountCondition::LessThan;
					return true;
				}
				else
					return false;
			}
		}
	}
	else if (type == "Title")
	{
		discountType = ItemDiscountType::Title;
		this->condition1 = condition1;
		this->condition2 = DiscountCondition::Equal;
		return true;
	}
	else
		return false;
}

void ItemDiscount::ApplyDiscounts(std::vector<Book*> books)
{
	for each(Book* book in books)
	{
		switch (discountType)
		{
		case Year:
			switch (condition2)
			{
			case Equal:
				if (stoi(condition1) == book->GetYear())
					book->ApplyDiscount(percent);
				break;
			case GreaterThan:
				if (stoi(condition1) < book->GetYear())
					book->ApplyDiscount(percent);
				break;
			case LessThan:
				if (stoi(condition1) > book->GetYear())
					book->ApplyDiscount(percent);
				break;
			case GreaterThanEqualTo:
				if (stoi(condition1) <= book->GetYear())
					book->ApplyDiscount(percent);
				break;
			case LessThanEqualTo:
				if (stoi(condition1) >= book->GetYear())
					book->ApplyDiscount(percent);
				break;
			}

			break;
		case Title:
			if (book->GetTitle() == condition1)
			{
				book->ApplyDiscount(percent);
			}
			break;
		}
	}
}