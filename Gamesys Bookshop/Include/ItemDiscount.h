#pragma once
#include <string>
#include <vector>
#include <Book.h>
#include <Enumerations.h>

enum ItemDiscountType
{
	Year,
	Title
};

class ItemDiscount
{
public:
	ItemDiscount();
	bool Init(std::string type, std::string condition1, std::string condition2, int percent);
	void ApplyDiscounts(std::vector<Book*> books);
private:
	ItemDiscountType discountType;
	std::string condition1;
	DiscountCondition condition2;
	int percent;
};