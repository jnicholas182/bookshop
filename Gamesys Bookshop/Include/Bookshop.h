#pragma once
#include <vector>
#include <Book.h>
#include <Basket.h>
#include <ItemDiscount.h>

class Bookshop
{
public:
	Bookshop();
	void Run();
	void Stop();
private:
	void Checkout();
	unsigned int longestStringLength;
	std::vector<Book*> books;
	std::vector<ItemDiscount*> itemDiscounts;
	Basket* basket;
};