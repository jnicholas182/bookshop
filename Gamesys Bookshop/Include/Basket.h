#pragma once
#include <vector>
#include <Book.h>
#include <BasketDiscount.h>

class Basket
{
public:
	Basket();
	void AddItem(Book* book);
	void RemoveItem(int index);
	void CalculateTotal();
	std::vector<Book*> GetBooksInBasket();
	void AddDiscount(BasketDiscount* discount);
private:
	std::vector<Book*> books;
	std::vector<BasketDiscount*> discounts;
	float total;
	int count;
};