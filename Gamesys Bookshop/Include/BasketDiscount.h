#pragma once
#include <string>
#include <vector>
#include <Book.h>
#include <Enumerations.h>

enum BasketDiscountType
{
	Amount,
	Count
};

class BasketDiscount
{
public:
	BasketDiscount();
	bool Init(std::string type, std::string condition1, std::string condition2, int percent);
	bool ApplyDiscount(float &total, int count);
private:
	BasketDiscountType discountType;
	std::string condition1;
	DiscountCondition condition2;
	int percent;
};