#pragma once

enum DiscountCondition
{
	Equal,
	GreaterThan,
	LessThan,
	GreaterThanEqualTo,
	LessThanEqualTo
};