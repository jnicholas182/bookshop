#pragma once
#include <string>

class Book
{
public:
	Book();
	bool Init(std::string title, std::string year, std::string price);
	void ApplyDiscount(int percent);
	std::string GetTitle();
	int GetYear();
	float GetPrice();
	void Print(int longestStringLength);
private:
	std::string title;
	int year;
	float price;
	int discount;
};